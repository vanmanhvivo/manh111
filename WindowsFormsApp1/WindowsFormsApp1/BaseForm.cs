﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Imaging.Filters;

namespace WindowsFormsApp1
{
    public partial class BaseForm : Form
    {
        protected Bitmap imgSource;
        protected Bitmap imgResult;


        public BaseForm()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void BaseForm_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void c_Click(object sender, EventArgs e)
        {

        }

        private void txtsave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Bitmap files |*.bmp| JPEG files|*.jpg| PNG file|*.png";
            saveFileDialog1.FileName = "";
            DialogResult r = saveFileDialog1.ShowDialog();
            if (r == DialogResult.OK && saveFileDialog1.FileName != "" && imgSource != null)
            {
                imgSource.Save(saveFileDialog1.FileName);
            }
        }
        private void txtopen_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Bitmap files |*.bmp| JPEG files|*.jpg| PNG file|*.png";
            openFileDialog1.FileName = "";
            DialogResult r = openFileDialog1.ShowDialog();
            if (r == DialogResult.OK && openFileDialog1.FileName != "")
            {
                txthienthi.Text = openFileDialog1.FileName;
                imgSource = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = imgSource;
                pictureBox1.Refresh();


                imgResult = new Bitmap(imgSource);
                pictureBox2.Image = imgResult;
                pictureBox2.Refresh();




            }

        }

        private void mouse(object sender, MouseEventArgs e)
        {
            txtx.Text = e.X.ToString();
            txty.Text = e.Y.ToString();
            if (imgSource == null)
                return;
            if (e.X < imgSource.Width && e.Y < imgSource.Height)
            {
                Color c = imgSource.GetPixel(e.X, e.Y);
                txtz.Text = c.R.ToString();
                txtg.Text = c.G.ToString();
                txtB.Text = c.B.ToString();



            }
        }

        private void Mousesave(object sender, MouseEventArgs e)
        {
            txtx2.Text = e.X.ToString();
            txty2.Text = e.Y.ToString();
            if (imgSource == null)
                return;
            if (e.X < imgSource.Width && e.Y < imgSource.Height)
            {
                Color c = imgSource.GetPixel(e.X, e.Y);
                txtz2.Text = c.R.ToString();
                txtG2.Text = c.G.ToString();
                txtB2.Text = c.B.ToString();



            }

        }

      
    }
}