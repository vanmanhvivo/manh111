﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Tach_mau : BaseForm
    {
        public Tach_mau()
        {
            InitializeComponent();
            //Bitmap red = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            //Bitmap green = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            //Bitmap blue = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            //for (int x = 0; x < pictureBox1.Width; x++)
            //    for (int y = 0; y < pictureBox1.Height; y++)
            //    {
            //        Color pixel = imgSource.GetPixel(x, y);
            //        byte R = pixel.R;
            //        byte G = pixel.G;
            //        byte B = pixel.B;
            //        byte A = pixel.A;
                   
                   

                
        }

        private void Tach_mau_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void rdored_CheckedChanged(object sender, EventArgs e)
        {
            for (int x = 1; x < imgSource.Width ; x++)
                for (int y = 1; y < imgSource.Height; y++)
                {
                    Color pixel = imgSource.GetPixel(x, y);
                    byte R = pixel.R;
                    imgResult.SetPixel(x, y, Color.FromArgb( R,0, 0));
                 
                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }

        private void rdogreen_CheckedChanged(object sender, EventArgs e)
        {
            for (int x = 1; x < imgSource.Width; x++)
                for (int y = 1; y < imgSource.Height; y++)
                {
                    Color pixel = imgSource.GetPixel(x, y);
                    byte R = pixel.R;
                    imgResult.SetPixel(x, y, Color.FromArgb(0, R, 0));

                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }

        private void rdoblue_CheckedChanged(object sender, EventArgs e)
        {
            for (int x = 1; x < imgSource.Width; x++)
                for (int y = 1; y < imgSource.Height; y++)
                {
                    Color pixel = imgSource.GetPixel(x, y);
                    byte R = pixel.R;
                    imgResult.SetPixel(x, y, Color.FromArgb(0, 0, R));

                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }
    }
}
