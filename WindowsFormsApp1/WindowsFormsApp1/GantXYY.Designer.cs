﻿
namespace WindowsFormsApp1
{
    partial class GantXYY
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGanty = new System.Windows.Forms.Button();
            this.BtnGantX = new System.Windows.Forms.Button();
            this.btnamban = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGanty
            // 
            this.btnGanty.Location = new System.Drawing.Point(329, 387);
            this.btnGanty.Name = "btnGanty";
            this.btnGanty.Size = new System.Drawing.Size(75, 23);
            this.btnGanty.TabIndex = 28;
            this.btnGanty.Text = "Ganty";
            this.btnGanty.UseVisualStyleBackColor = true;
            this.btnGanty.Click += new System.EventHandler(this.btnGanty_Click);
            // 
            // BtnGantX
            // 
            this.BtnGantX.Location = new System.Drawing.Point(207, 387);
            this.BtnGantX.Name = "BtnGantX";
            this.BtnGantX.Size = new System.Drawing.Size(75, 23);
            this.BtnGantX.TabIndex = 30;
            this.BtnGantX.Text = "GantX";
            this.BtnGantX.UseVisualStyleBackColor = true;
            this.BtnGantX.Click += new System.EventHandler(this.BtnGantX_Click);
            // 
            // btnamban
            // 
            this.btnamban.Location = new System.Drawing.Point(469, 387);
            this.btnamban.Name = "btnamban";
            this.btnamban.Size = new System.Drawing.Size(75, 23);
            this.btnamban.TabIndex = 31;
            this.btnamban.Text = "Am ban";
            this.btnamban.UseVisualStyleBackColor = true;
            this.btnamban.Click += new System.EventHandler(this.btnamban_Click);
            // 
            // GantXYY
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnamban);
            this.Controls.Add(this.BtnGantX);
            this.Controls.Add(this.btnGanty);
            this.Name = "GantXYY";
            this.Text = "GantXYY";
            this.Load += new System.EventHandler(this.GantXYY_Load);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            this.Controls.SetChildIndex(this.btnGanty, 0);
            this.Controls.SetChildIndex(this.BtnGantX, 0);
            this.Controls.SetChildIndex(this.btnamban, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGanty;
        private System.Windows.Forms.Button BtnGantX;
        private System.Windows.Forms.Button btnamban;
    }
}