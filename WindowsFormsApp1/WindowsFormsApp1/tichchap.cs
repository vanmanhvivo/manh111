﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class tichchap : BaseForm
    {
        public tichchap()
        {
            InitializeComponent();
        }

        private void tichchap_Load(object sender, EventArgs e)
        {

        }
        //private void SafeImageConvolution(Bitmap image, ConvMatrix fmat)
        //{
        //    //Avoid division by 0 
        //    if (fmat.Factor == 0)
        //        return;

        //    Bitmap srcImage = (Bitmap)image.Clone();

        //    int x, y, filterx, filtery;
        //    int s = fmat.Size / 2;
        //    int r, g, b;
        //    Color tempPix;

        //    for (y = s; y < srcImage.Height - s; y++)
        //    {
        //        for (x = s; x < srcImage.Width - s; x++)
        //        {
        //            r = g = b = 0;

        //            // Convolution 
        //            for (filtery = 0; filtery < fmat.Size; filtery++)
        //            {
        //                for (filterx = 0; filterx < fmat.Size; filterx++)
        //                {
        //                    tempPix = srcImage.GetPixel(x + filterx - s, y + filtery - s);

        //                    r += fmat.Matrix[filtery, filterx] * tempPix.R;
        //                    g += fmat.Matrix[filtery, filterx] * tempPix.G;
        //                    b += fmat.Matrix[filtery, filterx] * tempPix.B;
        //                }
        //            }

        //            r = Math.Min(Math.Max((r / fmat.Factor) + fmat.Offset, 0), 255);
        //            g = Math.Min(Math.Max((g / fmat.Factor) + fmat.Offset, 0), 255);
        //            b = Math.Min(Math.Max((b / fmat.Factor) + fmat.Offset, 0), 255);

        //            image.SetPixel(x, y, Color.FromArgb(r, g, b));
        //        }
        //    }
        //}

        private void btnRun_Click(object sender, EventArgs e)
        {
            int[] arr = new int[9];
            int[] arr2 = new int[9] { 1, 0, 0, 0, 1, 1, 1, 0, 1 };

            for (int x = 0; x < imgSource.Width - 2; x++)
                for (int y = 0; y < imgSource.Height - 2; y++)
                {
                    arr[0] = imgSource.GetPixel(x, y).R;
                    arr[1] = imgSource.GetPixel(x, y + 1).R;
                    arr[2] = imgSource.GetPixel(x, y + 2).R;

                    arr[3] = imgSource.GetPixel(x + 1, y).R;
                    arr[4] = imgSource.GetPixel(x + 1, y + 1).R;
                    arr[5] = imgSource.GetPixel(x + 1, y + 2).R;

                    arr[6] = imgSource.GetPixel(x + 2, y).R;
                    arr[7] = imgSource.GetPixel(x + 2, y + 1).R;
                    arr[8] = imgSource.GetPixel(x + 2, y + 2).R;
                    arr[0] = arr[0] * arr2[0] + arr[1] * arr2[1] + arr[2] * arr2[2] + arr[3] * arr2[3] +
                        arr[4] * arr2[4] + arr[5] * arr2[5] + arr[6] * arr2[6] + arr[7] * arr2[7] +
                        arr[8] * arr2[8];
                    if (arr[0] < 0)

                        imgResult.SetPixel(x, y, Color.FromArgb(0, 0, 0));
                    else if (arr[0] > 255)
                        imgResult.SetPixel(x, y, Color.FromArgb(255, 255, 255));
                    else
                        imgResult.SetPixel(x, y, Color.FromArgb(arr[0], arr[0], arr[0]));
                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }
    }
    }

