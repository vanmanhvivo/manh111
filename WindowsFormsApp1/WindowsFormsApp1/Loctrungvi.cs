﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Loctrungvi : BaseForm
    {
        public Loctrungvi()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ntnrunn_Click(object sender, EventArgs e)
        {
            int a = Convert.ToInt32(numericUpDown2.Value);
            int[] arr = new int[9];
            for (int x = 1; x < imgSource.Width - 1; x++)
                for (int y = 1; y < imgSource.Height - 1; y++)
                {
                    arr[0] = imgSource.GetPixel(x - 1, y - 1).R;
                    arr[1] = imgSource.GetPixel(x, y - 1).R;
                    arr[2] = imgSource.GetPixel(x + 1, y - 1).R;

                    arr[3] = imgSource.GetPixel(x - 1, y).R;
                    arr[4] = imgSource.GetPixel(x, y).R;
                    arr[5] = imgSource.GetPixel(x + 1, y).R;

                    arr[6] = imgSource.GetPixel(x - 1, y + 1).R;
                    arr[7] = imgSource.GetPixel(x, y + 1).R;
                    arr[8] = imgSource.GetPixel(x + 1, y + 1).R;

                    //xắp xêp
                    int tpm = 0;
                    for (int i=0;i<8;i++)
                        for (int j=i+1; j<9;j++)
                            if (arr [i]<arr[j])
                            {
                                tpm = arr[i];
                                arr[i] = arr[j];
                                arr[j] = tpm;

                            }
                    //Tính giá trị trung vị
                    int media_value = arr[4];
                    //xac đinh có thay thêd mức xám tại pixel (x,y) hay kông
                    if(Math.Abs(imgSource.GetPixel(x,y).R - media_value)>a)
                    {
                        Color b = Color.FromArgb(media_value, media_value, media_value);
                        imgResult.SetPixel(x,y, b);
                    }    
                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }
    }
}
