﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class GantXYY : BaseForm
    {
        public GantXYY()
        {
            InitializeComponent();
        }

        private void GantXYY_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void BtnGantX_Click(object sender, EventArgs e)
        {
            Color c1;
            for(int x=1;x<imgResult.Width-1;x++)
                for(int y=0; y< imgResult.Height;y++)
                {
                    int gx = Math.Abs(imgSource.GetPixel(x + 1, y).R - imgSource.GetPixel(x - 1, y).R);
                    //int gy = Math.Abs(imgSource.GetPixel(x + 1, y).R - imgSource.GetPixel(x - 1, y).R);

                    gx = 255 - gx;
                    c1 = Color.FromArgb(gx, gx, gx);
                    imgResult.SetPixel(x, y, c1);
                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }

        private void btnGanty_Click(object sender, EventArgs e)
        {
            Color c1;
            for (int x = 1; x < imgResult.Width - 1; x++)
                for (int y = 0; y < imgResult.Height; y++)
                {
                    int gx = Math.Abs(imgSource.GetPixel(x + 1, y).R - imgSource.GetPixel(x - 1, y).R);
                    int gy = Math.Abs(imgSource.GetPixel(x + 1, y).R - imgSource.GetPixel(x - 1, y).R);
                    int g = (int)Math.Sqrt(gx * gx + gy * gy);
                     g = Math.Min(g,255);
                    gx = 255 - g;
                    c1 = Color.FromArgb(g, g, g);
                    imgResult.SetPixel(x, y, c1);
                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }

        private void btnamban_Click(object sender, EventArgs e)
        {
            Color c;
            for (int x = 0; x < imgResult.Width; x++)
                for (int y = 0; y < imgResult.Height; y++)
                {
                    c = imgResult.GetPixel(x, y);
                  imgResult.SetPixel(x,y,Color.FromArgb(255-c.R,255-c.G,255-c.B));
                   
                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }
    }
}
