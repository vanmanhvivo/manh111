﻿
namespace WindowsFormsApp1
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblhienthi = new System.Windows.Forms.Label();
            this.txthienthi = new System.Windows.Forms.TextBox();
            this.lblx = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtx = new System.Windows.Forms.TextBox();
            this.txty = new System.Windows.Forms.TextBox();
            this.txtz = new System.Windows.Forms.TextBox();
            this.txtsave = new System.Windows.Forms.Button();
            this.txtz2 = new System.Windows.Forms.TextBox();
            this.txty2 = new System.Windows.Forms.TextBox();
            this.txtx2 = new System.Windows.Forms.TextBox();
            this.red = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txtopen = new System.Windows.Forms.Button();
            this.txtB = new System.Windows.Forms.TextBox();
            this.txtg = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.txtG2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.pictureBox1.Location = new System.Drawing.Point(54, 76);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(214, 178);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mouse);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox2.Location = new System.Drawing.Point(442, 76);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(204, 178);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mousesave);
            // 
            // lblhienthi
            // 
            this.lblhienthi.AutoSize = true;
            this.lblhienthi.Location = new System.Drawing.Point(93, 44);
            this.lblhienthi.Name = "lblhienthi";
            this.lblhienthi.Size = new System.Drawing.Size(0, 13);
            this.lblhienthi.TabIndex = 2;
            // 
            // txthienthi
            // 
            this.txthienthi.Location = new System.Drawing.Point(54, 27);
            this.txthienthi.Name = "txthienthi";
            this.txthienthi.Size = new System.Drawing.Size(282, 20);
            this.txthienthi.TabIndex = 4;
            // 
            // lblx
            // 
            this.lblx.AutoSize = true;
            this.lblx.Location = new System.Drawing.Point(297, 74);
            this.lblx.Name = "lblx";
            this.lblx.Size = new System.Drawing.Size(12, 13);
            this.lblx.TabIndex = 5;
            this.lblx.Text = "x";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(297, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "y";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(300, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "red";
            // 
            // txtx
            // 
            this.txtx.Location = new System.Drawing.Point(338, 74);
            this.txtx.Name = "txtx";
            this.txtx.Size = new System.Drawing.Size(79, 20);
            this.txtx.TabIndex = 8;
            this.txtx.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txty
            // 
            this.txty.Location = new System.Drawing.Point(338, 103);
            this.txty.Name = "txty";
            this.txty.Size = new System.Drawing.Size(79, 20);
            this.txty.TabIndex = 9;
            // 
            // txtz
            // 
            this.txtz.Location = new System.Drawing.Point(341, 130);
            this.txtz.Name = "txtz";
            this.txtz.Size = new System.Drawing.Size(76, 20);
            this.txtz.TabIndex = 10;
            // 
            // txtsave
            // 
            this.txtsave.Location = new System.Drawing.Point(533, 294);
            this.txtsave.Name = "txtsave";
            this.txtsave.Size = new System.Drawing.Size(75, 23);
            this.txtsave.TabIndex = 11;
            this.txtsave.Text = "save";
            this.txtsave.UseVisualStyleBackColor = true;
            this.txtsave.Click += new System.EventHandler(this.txtsave_Click);
            // 
            // txtz2
            // 
            this.txtz2.Location = new System.Drawing.Point(703, 132);
            this.txtz2.Name = "txtz2";
            this.txtz2.Size = new System.Drawing.Size(76, 20);
            this.txtz2.TabIndex = 17;
            // 
            // txty2
            // 
            this.txty2.Location = new System.Drawing.Point(700, 105);
            this.txty2.Name = "txty2";
            this.txty2.Size = new System.Drawing.Size(79, 20);
            this.txty2.TabIndex = 16;
            // 
            // txtx2
            // 
            this.txtx2.Location = new System.Drawing.Point(700, 76);
            this.txtx2.Name = "txtx2";
            this.txtx2.Size = new System.Drawing.Size(79, 20);
            this.txtx2.TabIndex = 15;
            // 
            // red
            // 
            this.red.AutoSize = true;
            this.red.Location = new System.Drawing.Point(662, 132);
            this.red.Name = "red";
            this.red.Size = new System.Drawing.Size(22, 13);
            this.red.TabIndex = 14;
            this.red.Text = "red";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(659, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "y";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(659, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "x";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtopen
            // 
            this.txtopen.Location = new System.Drawing.Point(224, 294);
            this.txtopen.Name = "txtopen";
            this.txtopen.Size = new System.Drawing.Size(75, 23);
            this.txtopen.TabIndex = 19;
            this.txtopen.Text = "open";
            this.txtopen.UseVisualStyleBackColor = true;
            this.txtopen.Click += new System.EventHandler(this.txtopen_Click);
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(344, 183);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(76, 20);
            this.txtB.TabIndex = 23;
            // 
            // txtg
            // 
            this.txtg.Location = new System.Drawing.Point(341, 156);
            this.txtg.Name = "txtg";
            this.txtg.Size = new System.Drawing.Size(79, 20);
            this.txtg.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(300, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Blue";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(300, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "green";
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(706, 185);
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(76, 20);
            this.txtB2.TabIndex = 27;
            // 
            // txtG2
            // 
            this.txtG2.Location = new System.Drawing.Point(703, 158);
            this.txtG2.Name = "txtG2";
            this.txtG2.Size = new System.Drawing.Size(79, 20);
            this.txtG2.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(665, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Blue";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(662, 158);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Green";
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 500);
            this.Controls.Add(this.txtB2);
            this.Controls.Add(this.txtG2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtB);
            this.Controls.Add(this.txtg);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtopen);
            this.Controls.Add(this.txtz2);
            this.Controls.Add(this.txty2);
            this.Controls.Add(this.txtx2);
            this.Controls.Add(this.red);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtsave);
            this.Controls.Add(this.txtz);
            this.Controls.Add(this.txty);
            this.Controls.Add(this.txtx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblx);
            this.Controls.Add(this.txthienthi);
            this.Controls.Add(this.lblhienthi);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "BaseForm";
            this.Text = "BaseForm";
            this.Load += new System.EventHandler(this.BaseForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblhienthi;
        private System.Windows.Forms.TextBox txthienthi;
        private System.Windows.Forms.Label lblx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtx;
        private System.Windows.Forms.TextBox txty;
        private System.Windows.Forms.TextBox txtz;
        private System.Windows.Forms.Button txtsave;
        private System.Windows.Forms.TextBox txtz2;
        private System.Windows.Forms.TextBox txty2;
        private System.Windows.Forms.TextBox txtx2;
        private System.Windows.Forms.Label red;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button txtopen;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.TextBox txtG2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.PictureBox pictureBox2;
    }
}