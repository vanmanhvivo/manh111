﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace WindowsFormsApp1
{
    public partial class Biểu_đò_mức_xám : BaseForm
    {
        public Biểu_đò_mức_xám()
        {
            InitializeComponent(); 
           // Bitmap hinhgoc= new Bitmap(@"C:\Users\Admin\Desktop\Image\Image");
            //double[] histogram = tinhhistogram(imgSource);
            //PointPairList points = Chuyendothi(histogram);
            //zedGraphControl1.GraphPane = Bieudo(points);
            //zedGraphControl1.Refresh();
        }
        
        private void Biểu_đò_mức_xám_Load(object sender, EventArgs e)
        {
      
        }
        //public double[] tinhhistogram(Bitmap imgSource)
        //{
        //    double[] histogram = new double[256];
        //    for (int x = 1; x < imgSource.Width; x++)
        //        for (int y = 1; y < imgSource.Height; y++)
        //        {
        //            Color pixel = imgSource.GetPixel(x, y);
        //            byte R = pixel.R;
        //            histogram[R]++;
        //        }
        //    return histogram;
        //        }
        //PointPairList Chuyendothi(double[] histogram)
        //{
        //    PointPairList points = new PointPairList();
        //    for (int x = 1; x < histogram.Length; x++)
        //    {
        //        points.Add(x, histogram[x]);
        //    }
        //    return points;
        //}
        public GraphPane Bieudo (PointPairList histogram)
        {
            GraphPane gp = new GraphPane();
            gp.Title.Text = @"Biểu đồ histogram";
            gp.Rect = new Rectangle(0, 0, 700, 500);
            gp.XAxis.Title.Text = @"Giá trị mức xám của các điểm ảnh";
            gp.XAxis.Scale.Min = 0;
            gp.XAxis.Scale.Min = 255;
            gp.XAxis.Scale.MajorStep = 5;
            gp.XAxis.Scale.MinorStep = 1;


            gp.XAxis.Title.Text = @"Số điển ảnh cùng mức xám";
            gp.XAxis.Scale.Min = 0;
            gp.XAxis.Scale.Min = 15000;
            gp.XAxis.Scale.MajorStep = 5;
            gp.XAxis.Scale.MinorStep = 1;
            gp.AddBar("Histogram", histogram, Color.OrangeRed);
            return gp;
        }
        public static Bitmap HistEq(Bitmap img)
        {
            int w = img.Width;
            int h = img.Height;
            BitmapData sd = img.LockBits(new Rectangle(0, 0, w, h),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            int bytes = sd.Stride * sd.Height;
            byte[] buffer = new byte[bytes];
            byte[] result = new byte[bytes];
            Marshal.Copy(sd.Scan0, buffer, 0, bytes);
            img.UnlockBits(sd);
            int current = 0;
            double[] pn = new double[256];
            for (int p = 0; p < bytes; p += 4)
            {
                pn[buffer[p]]++;
            }
            for (int prob = 0; prob < pn.Length; prob++)
            {
                pn[prob] /= (w * h);
            }
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    current = y * sd.Stride + x * 4;
                    double sum = 0;
                    for (int i = 0; i < buffer[current]; i++)
                    {
                        sum += pn[i];
                    }
                    for (int c = 0; c < 3; c++)
                    {
                        result[current + c] = (byte)Math.Floor(255 * sum);
                    }
                    result[current + 3] = 255;
                }
            }
            Bitmap res = new Bitmap(w, h);
            BitmapData rd = res.LockBits(new Rectangle(0, 0, w, h),
                ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(result, 0, rd.Scan0, bytes);
            res.UnlockBits(rd);
            return res;
        }

    }
}
