﻿
namespace WindowsFormsApp1
{
    partial class Tach_mau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdored = new System.Windows.Forms.RadioButton();
            this.rdogreen = new System.Windows.Forms.RadioButton();
            this.rdoblue = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // rdored
            // 
            this.rdored.AutoSize = true;
            this.rdored.Location = new System.Drawing.Point(361, 40);
            this.rdored.Name = "rdored";
            this.rdored.Size = new System.Drawing.Size(40, 17);
            this.rdored.TabIndex = 28;
            this.rdored.TabStop = true;
            this.rdored.Text = "red";
            this.rdored.UseVisualStyleBackColor = true;
            this.rdored.CheckedChanged += new System.EventHandler(this.rdored_CheckedChanged);
            // 
            // rdogreen
            // 
            this.rdogreen.AutoSize = true;
            this.rdogreen.Location = new System.Drawing.Point(452, 40);
            this.rdogreen.Name = "rdogreen";
            this.rdogreen.Size = new System.Drawing.Size(52, 17);
            this.rdogreen.TabIndex = 28;
            this.rdogreen.TabStop = true;
            this.rdogreen.Text = "green";
            this.rdogreen.UseVisualStyleBackColor = true;
            this.rdogreen.CheckedChanged += new System.EventHandler(this.rdogreen_CheckedChanged);
            // 
            // rdoblue
            // 
            this.rdoblue.AutoSize = true;
            this.rdoblue.Location = new System.Drawing.Point(543, 40);
            this.rdoblue.Name = "rdoblue";
            this.rdoblue.Size = new System.Drawing.Size(45, 17);
            this.rdoblue.TabIndex = 28;
            this.rdoblue.TabStop = true;
            this.rdoblue.Text = "blue";
            this.rdoblue.UseVisualStyleBackColor = true;
            this.rdoblue.CheckedChanged += new System.EventHandler(this.rdoblue_CheckedChanged);
            // 
            // Tach_mau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 504);
            this.Controls.Add(this.rdoblue);
            this.Controls.Add(this.rdogreen);
            this.Controls.Add(this.rdored);
            this.Name = "Tach_mau";
            this.Text = "Tach_mau";
            this.Load += new System.EventHandler(this.Tach_mau_Load);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            this.Controls.SetChildIndex(this.rdored, 0);
            this.Controls.SetChildIndex(this.rdogreen, 0);
            this.Controls.SetChildIndex(this.rdoblue, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdored;
        private System.Windows.Forms.RadioButton rdogreen;
        private System.Windows.Forms.RadioButton rdoblue;
    }
}