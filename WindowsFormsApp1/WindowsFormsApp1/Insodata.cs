﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Insodata : BaseForm
    {
        public Insodata()
        {
            InitializeComponent();
        }

        private void Insodata_Load(object sender, EventArgs e)
        {

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            txtxem.Text = trackBar1.Value.ToString();
            Color c;
            for (int i = 0; i < imgSource.Width; i++)
                for (int j = 0; j < imgSource.Height; j++)
                {
                    if (imgSource.GetPixel(i, j).R > trackBar1.Value)
                        c = Color.FromArgb(255, 255, 255);
                    else
                        c = Color.FromArgb(0, 0, 0);
                    imgResult.SetPixel(i, j, c);

                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }
        private byte Timnguong(Bitmap img)
        {
            byte th0 = 128;
            byte th1 = 0;
            while (1==1)
            {
                int tong0 = 0;int dem0 = 0; int tong1 = 0;int dem1 = 0;
                for (int x = 0; x < img.Width; x++)
                    for (int y=0;y<img.Height;y++)

                {
                        Color c = img.GetPixel(x, y);
                        if(c.R>th0)
                        {
                            tong1 = tong1 + c.R;
                            dem1++;
                        }    
                        else
                        {
                            tong0 = tong0 + c.R;dem0++ ;
                        }
                   
                }
                if (dem0 == 0) dem0 = 1;
                if (dem1 == 0) dem1 = 1;
                // math.round
                th1 = (byte)((tong1 / dem1 + tong0 / dem0) / 2);
                if (Math.Abs(th1 - th0) < 2)
                    break;
                else
                    th0 = th1;
            }
            return th0;
        }

        private void btnrun_Click(object sender, EventArgs e)
        {
            int thres = Timnguong(imgSource);
            txtxem.Text = thres.ToString();
            Color c;
            for (int i = 0; i < imgSource.Width; i++)
                for (int j = 0; j < imgSource.Height; j++)
                {
                    if (imgSource.GetPixel(i, j).R > thres)
                        c = Color.FromArgb(255, 255, 255);
                    else
                        c = Color.FromArgb(0, 0, 1);
                    imgResult.SetPixel(i, j, c);
                    imgResult.SetPixel(i, j, c);

                }
            pictureBox2.Image = imgResult;
            pictureBox2.Refresh();
        }

        private void txtxem_TextChanged(object sender, EventArgs e)
        {

        }
    }
    
}
